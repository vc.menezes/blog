<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'identifier'   => (int)$category->id,
            'title'        => (string)$category->name,
            'details'      => (string)$category->description,
            'creationDate' => (string)$category->created_at,
            'lastChange'   => (string)$category->updated_at,
            'deletedDate'  => isset($category->deleted_at) ? (string)  $category->deleted_at : null,
            //HATEOAS
            'links'        => [
                [
                'rel'  => 'self',
                'href' => route('categories.show', $category->id)
                ],
                [
                'rel'  => 'category.visitors',
                'href' => route('categories.visitors.index', $category->id)
                ],
                [
                'rel'  => 'category.posts',
                'href' => route('categories.posts.index', $category->id)
                ],
                [
                'rel'  => 'category.authors',
                'href' => route('categories.authors.index', $category->id)
                ],
                [
                'rel'  => 'category.comments',
                'href' => route('categories.comments.index', $category->id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier'   => 'id',
            'title'        => 'name',
            'details'      => 'description',
            'creationDate' => 'created_at',
            'lastChange'   => 'updated_at',
            'deletedDate'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
          'id'          => 'identifier',              
          'name'        => 'title',              
          'description' => 'details',              
          'created_at'  => 'creationDate',                         
          'updated_at'  => 'lastChange',                         
          'deleted_at'  => 'deletedDate',                         
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
