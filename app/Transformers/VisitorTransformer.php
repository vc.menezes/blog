<?php

namespace App\Transformers;

use App\Visitor;
use League\Fractal\TransformerAbstract;

class VisitorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Visitor $visitor)
    {
        return [
            'identifier'   => (int)$visitor->id,
            'name'         => (string)$visitor->name,
            'email'        => (string)$visitor->email,
            'isVerified'   => (int)$visitor->verified,
            'creationDate' => (string)$visitor->created_at,
            'lastChange'   => (string)$visitor->updated_at,
            'deletedDate'  => isset($visitor->deleted_at) ? (string)  $visitor->deleted_at : null,
            'links'        => [
                [
                'rel'  => 'self',
                'href' => route('visitors.show', $visitor->id)
                ],
                [
                'rel'  => 'visitor.posts',
                'href' => route('visitors.posts.index', $visitor->id)
                ],
                [
                'rel'  => 'visitor.categories',
                'href' => route('visitors.categories.index', $visitor->id)
                ],
                [
                'rel'  => 'visitor.comments',
                'href' => route('visitors.comments.index', $visitor->id)
                ],
                [
                'rel'  => 'visitor.author',
                'href' => route('visitors.authors.index', $visitor->id)
                ],
                [
                'rel'  => 'visitor.user',
                'href' => route('users.show', $visitor->id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier'   => 'id',
            'name'         => 'name',
            'email'        => 'email',
            'isVerified'   => 'verified',
            'creationDate' => 'created_at',
            'lastChange'   => 'updated_at',
            'deletedDate'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
         'id'         => 'identifier',
         'name'       => 'name',
         'email'      => 'email',
         'verified'   => 'isVerified',
         'created_at' => 'creationDate',
         'updated_at' => 'lastChange',
         'deleted_at' => 'deletedDate',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
