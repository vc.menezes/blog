<?php

namespace App\Transformers;

use App\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'identifier'   => (int)$post->id,
            'name'         => (string)$post->title,
            'text'         => (string)$post->content,
            'situation'    => (string)$post->status,
            'picture'      => url("img/{$post->attachment}"),
            'author'       => (int)$post->author_id,
            'creationDate' => (string)$post->created_at,
            'lastChange'   => (string)$post->updated_at,
            'deletedDate'  => isset($post->deleted_at) ? (string)  $post->deleted_at : null,

            'links'        => [
                [
                'rel'  => 'self',
                'href' => route('posts.show', $post->id)
                ],
                [
                'rel'  => 'post.visitors',
                'href' => route('posts.visitors.index', $post->id)
                ],
                [
                'rel'  => 'post.categories',
                'href' => route('posts.categories.index', $post->id)
                ],
                [
                'rel'  => 'post.comments',
                'href' => route('posts.comments.index', $post->id)
                ],
                [
                'rel'  => 'author',
                'href' => route('authors.show', $post->author_id)
                ],
            ]
        ];


    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier'   => 'id',
            'name'         => 'title',
            'text'         => 'content',
            'situation'    => 'status',
            'picture'      => 'attachment',
            'author'       => 'author_id',
            'creationDate' => 'created_at',
            'lastChange'   => 'updated_at',
            'deletedDate'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id'         => 'identifier',         
            'title'      => 'name',         
            'content'    => 'text',         
            'status'     => 'situation',         
            'attachment' => 'picture',         
            'author_id'  => 'author',            
            'created_at' => 'creationDate',           
            'updated_at' => 'lastChange',           
            'deleted_at' => 'deletedDate',           
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
