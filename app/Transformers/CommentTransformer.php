<?php

namespace App\Transformers;

use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
            'identifier'   => (int)$comment->id,
            'name'         => (string)$comment->title,
            'text'         => (string)$comment->content,
            'post'         => (int)$comment->post_id,
            'visitor'      => (int)$comment->visitor_id,
            'creationDate' => (string)$comment->created_at,
            'lastChange'   => (string)$comment->updated_at,
            'deletedDate'  => isset($comment->deleted_at) ? (string)  $comment->deleted_at : null,

            'links'        => [
                [
                'rel'  => 'self',
                'href' => route('comments.show', $comment->id)
                ],
                [
                'rel'  => 'comment.categories',
                'href' => route('comments.categories.index', $comment->id)
                ],
                [
                'rel'  => 'comment.author',
                'href' => route('comments.authors.index', $comment->id)
                ],
                [
                'rel'  => 'visitor',
                'href' => route('visitors.show', $comment->visitor_id)
                ],
                [
                'rel'  => 'post',
                'href' => route('posts.show', $comment->post_id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier'   => 'id',
            'name'         => 'title',
            'text'         => 'content',
            'post'         => 'post_id',
            'visitor'      => 'visitor_id',
            'creationDate' => 'created_at',
            'lastChange'   => 'updated_at',
            'deletedDate'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

        public static function transformedAttribute($index)
    {
        $attributes = [
          'id'         => 'identifier',             
          'title'      => 'name',             
          'content'    => 'text',             
          'post_id'    => 'post',                       
          'visitor_id' => 'visitor',                    
          'created_at' => 'creationDate',                    
          'updated_at' => 'lastChange',                    
          'deleted_at' => 'deletedDate',                    
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
