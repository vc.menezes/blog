<?php

namespace App\Policies;

use App\Post;
use App\Traits\AdminActions;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization, AdminActions;


    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function addCategory(User $user, Post $post)
    {
        return $user->id === $post->author->id;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function deleteCategory(User $user, Post $post)
    {
        return $user->id === $post->author->id;
    }


}
