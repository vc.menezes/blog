<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class CommentCategoryController extends ApiController
{
    public function __construct()
    {
        
        $this->middleware('client.credentials')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Comment $comment)
    {
        $categories = $comment->post->categories;
        return $this->showAll($categories);
    }

}
