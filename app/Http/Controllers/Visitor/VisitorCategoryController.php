<?php

namespace App\Http\Controllers\Visitor;

use App\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class VisitorCategoryController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only(['index']);
        $this->middleware('can:view,visitor')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Visitor $visitor)
    {
        $categories = $visitor->comments()->with('post.categories')->get()->pluck('post.categories')->collapse()->unique('id')->values();
        return $this->showAll($categories);
    }

   
}
