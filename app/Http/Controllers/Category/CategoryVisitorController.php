<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoryVisitorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $this->allowedAdminAction();
        $visitors = $category->posts()->whereHas('comments')->with('comments.visitor')->get()->pluck('comments')->collapse()->pluck('visitor')->unique('id')->values();
        return $this->showAll($visitors);
    }

}
