<?php

namespace App\Http\Controllers\Author;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AuthorCommentController extends ApiController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only(['index']);
        $this->middleware('can:view,author')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Author $author)
    {
        $comments = $author->posts()->whereHas('comments')->with('comments')->get()->pluck('comments')->collapse();
        return $this->showAll($comments);
    }

}
