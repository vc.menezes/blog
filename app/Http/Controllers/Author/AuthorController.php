<?php

namespace App\Http\Controllers\Author;

use App\Author;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class AuthorController extends ApiController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only(['show']);
        $this->middleware('can:view,author')->only(['show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();
        $authors = Author::has('posts')->get();
        return $this->showAll($authors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        return $this->showOne($author);
    }

}
