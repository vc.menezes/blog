<?php

namespace App\Http\Controllers\Post;

use App\Comment;
use App\Http\Controllers\ApiController;
use App\Post;
use App\Transformers\CommentTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostVisitorCommentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . CommentTransformer::class)->only(['store']);
        $this->middleware('scope:comment-posts')->only(['store']);
        $this->middleware('can:commentPost,visitor')->only(['store']);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post, User $visitor)
    {
        $rules = [
            'title' => 'required|min:2|max:100',
            'content'=> 'required|min:2|max:500',
        ];

        $this->validate($request, $rules);

        if ($visitor->id == $post->author_id) {
            return $this->errorResponse('This visitor must be different from author!', 409);
        }

        if (!$visitor->isVerified()) {
           return $this->errorResponse('The visitor must be a verified user.', 409);
        }

        if (!$post->author->isVerified()) {
            return $this->errorResponse('The author must ve a verified user!', 409);
        }


        return DB::transaction(function() use ($request, $post, $visitor) {
            $post->save();

            $comment = Comment::create([
                'title' => $request->title,
                'content'=> $request->content,
                'visitor_id'=>$visitor->id,
                'post_id'=>$post->id,
            ]);

            return $this->showOne($comment, 201);
        });
    }

}
