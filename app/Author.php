<?php

namespace App;

use App\Post;
use App\Scopes\AuthorScope;
use App\Transformers\AuthorTransformer;


class Author extends User
{

	public $transformer = AuthorTransformer::class;

    protected static function boot()
    {
    	parent::boot();
    	static::addGlobalScope(new AuthorScope);
    }

    public function posts()
    {
    	return $this->hasMany(Post::class);
    }
}
