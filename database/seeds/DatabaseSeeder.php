<?php

use App\Category;
use App\Comment;
use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Category::truncate();
        Post::truncate();
        Comment::truncate();
		DB::table('category_post')->truncate();

		User::flushEventListeners();
		Category::flushEventListeners();
		Post::flushEventListeners();
		Comment::flushEventListeners();

		$usersQuantity = 250;
		$categoriesQuantity = 20;
		$postsQuantity = 200;
		$commentsQuantity = 600;

		factory(User::class, $usersQuantity)->create();
		factory(Category::class, $categoriesQuantity)->create();
		factory(Post::class, $postsQuantity)->create()->each(
			function ($post){
				$categories = Category::all()->random(mt_rand(1, 5))->pluck('id');
				$post->categories()->attach($categories);
			});
		factory(Comment::class, $commentsQuantity)->create();
    }
}
