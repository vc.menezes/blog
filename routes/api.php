<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
*Visitors
**/
Route::resource('visitors', 'Visitor\VisitorController', ['only'=>['index', 'show']]);
Route::resource('visitors.comments', 'Visitor\VisitorCommentController', ['only'=>['index']]);
Route::resource('visitors.posts', 'Visitor\VisitorPostController', ['only'=>['index']]);
Route::resource('visitors.authors', 'Visitor\VisitorAuthorController', ['only'=>['index']]);
Route::resource('visitors.categories', 'Visitor\VisitorCategoryController', ['only'=>['index']]);

/**
*Authors
**/
Route::resource('authors', 'Author\AuthorController', ['only'=>['index', 'show']]);
Route::resource('authors.comments', 'Author\AuthorCommentController', ['only'=>['index']]);
Route::resource('authors.categories', 'Author\AuthorCategoryController', ['only'=>['index']]);
Route::resource('authors.visitors', 'Author\AuthorVisitorController', ['only'=>['index']]);
Route::resource('authors.posts', 'Author\AuthorPostController', ['except'=>['create', 'edit', 'show']]);

/**
*Posts
**/
Route::resource('posts', 'Post\PostController', ['only'=>['index', 'show']]);
Route::resource('posts.comments', 'Post\PostCommentController', ['only'=>['index']]);
Route::resource('posts.visitors', 'Post\PostVisitorController', ['only'=>['index']]);
Route::resource('posts.categories', 'Post\PostCategoryController', ['only'=>['index', 'update', 'destroy']]);
Route::resource('posts.visitors.comments', 'Post\PostVisitorCommentController', ['only'=>['store']]);

/**
*Comments
**/
Route::resource('comments', 'Comment\CommentController', ['only'=>['index', 'show']]);
Route::resource('comments.categories', 'Comment\CommentCategoryController', ['only'=>['index']]);
Route::resource('comments.authors', 'Comment\CommentAuthorController', ['only'=>['index']]);

/**
*Categories
**/
Route::resource('categories', 'Category\CategoryController', ['except'=>['create', 'edit']]);
Route::resource('categories.posts', 'Category\CategoryPostController', ['only'=>['index']]);
Route::resource('categories.authors', 'Category\CategoryAuthorController', ['only'=>['index']]);
Route::resource('categories.comments', 'Category\CategoryCommentController', ['only'=>['index']]);
Route::resource('categories.visitors', 'Category\CategoryVisitorController', ['only'=>['index']]);

/**
*Users
**/
Route::get('users/me', 'User\UserController@me')->name('me');
Route::resource('users', 'User\UserController', ['except'=>['create', 'edit']]);
Route::get('users/verify/{token}', 'User\UserController@verify')->name('verify');
Route::get('users/{user}/resend', 'User\UserController@resend')->name('resend');

/**
*Oauth
**/
Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');